import { generateSW as _generateSw, injectManifest as _injectManifest } from 'workbox-build';
import prettyBytes from 'pretty-bytes';
import rollup from 'rollup';
import replace from '@rollup/plugin-replace';
import { terser } from 'rollup-plugin-terser';
import resolve from '@rollup/plugin-node-resolve';

const name = 'workbox';

const report = ({ swDest, count, size }) => {
  const prettySize = prettyBytes(size);

  console.log(`\nThe service worker file was written to ${swDest}`);
  console.log(`The service worker will precache ${count} URLs, totaling ${prettySize}.\n`);
};

/**
 * @typedef {Object} GenerateSWConfig - https://developers.google.com/web/tools/workbox/modules/workbox-build#generatesw_mode
 * @property {string} swDest - Indicates whether the Courage component is present.
 * @property {Object[]} [additionalManifestEntries] - A list of entries to be precached, in addition to any entries that are generated as part of the build configuration.
 * @property {string[]} [babelPresetEnvTargets] - The targets to pass to babel-preset-env when transpiling the service worker bundle.
 * @property {Boolean} [inlineWorkboxRuntime] - Whether the runtime code for the Workbox library should be included in the top-level service worker, or split into a separate file that needs to be deployed alongside the service worker.
 * @property {string} [mode] - If set to 'production', then an optimized service worker bundle that excludes debugging info will be produced.
 * @property {Boolean} [sourcemap] - Whether to create a sourcemap for the generated service worker files.
 * @property {Boolean} [skipWaiting] - Whether or not the service worker should skip over the waiting lifecycle stage.
 * @property {Boolean} [clientsClaim] - Whether or not the service worker should start controlling any existing clients as soon as it activates.
 * @property {Object[]} [runtimeCaching] - Passing in an array of objects containing urlPatterns, handlers, and potentially options will add the appropriate code to the generated service worker to handle runtime caching.
 * @property {string} [navigateFallback] - This will be used to create a NavigationRoute that will respond to navigation requests for URLs that that aren't precached.
 * @property {Object[]} [navigateFallbackDenylist] - An optional array of regular expressions that restricts which URLs the configured navigateFallback behavior applies to.
 * @property {Object[]} [navigateFallbackAllowlist] - An optional array of regular expressions that restricts which URLs the configured navigateFallback behavior applies to.
 * @property {string[]} [importScripts] - An required list of JavaScript files that should be passed to importScripts() inside the generated service worker file.
 * @property {Object[]} [ignoreURLParametersMatching] - Any search parameter names that match against one of the regex's in this array will be removed before looking for a precache match.
 * @property {string} [directoryIndex] - If a navigation request for a URL ending in / fails to match a precached URL, this value will be appended to the URL and that will be checked for a precache match.
 * @property {string} [cacheId] - An optional ID to be prepended to cache names used by Workbox.
 * @property {Boolean} [offlineGoogleAnalytics] - Controls whether or not to include support for offline Google Analytics.
 * @property {Boolean} [cleanupOutdatedCaches] - Whether or not Workbox should attempt to identify an delete any precaches created by older, incompatible versions.
 * @property {Boolean} [navigationPreload] - Whether or not to enable navigation preload in the generated service worker.
 * @property {string} globDirectory - The base directory you wish to match globPatterns against, relative to the current working directory.
 * @property {Boolean} [globFollow] - Determines whether or not symlinks are followed when generating the precache manifest.
 * @property {string[]} [globIgnores] - A set of patterns matching files to always exclude when generating the precache manifest.
 * @property {string[]} [globPatterns] - Files matching against any of these patterns will be included in the precache manifest.
 * @property {Boolean} [globStrict] - If true, an error reading a directory when generating a precache manifest will cause the build to fail. If false, the problematic directory will be skipped.
 * @property {Object} [templatedURLs] - If a URL is rendered generated based on some server-side logic, its contents may depend on multiple files or on some other unique string value.
 * @property {number} [maximumFileSizeToCacheInBytes] - This value can be used to determine the maximum size of files that will be precached. This prevents you from inadvertantly precaching very large files that might have accidentally matched one of your patterns.
 * @property {Object} [dontCacheBustURLsMatching] - Assets that match this regex will be assumed to be uniquely versioned via their URL, and exempted from the normal HTTP cache-busting that's done when populating the precache.
 * @property {Object} [modifyURLPrefix] - A mapping of prefixes that, if present in an entry in the precache manifest, will be replaced with the corresponding value.
 * @property {Array} [manifestTransforms] - One or more ManifestTransform functions, which will be applied sequentially against the generated manifest.
 */

/**
 * Build me a service-worker
 * @param  {GenerateSWConfig} generateSWConfig
 * @param  {Function} render - Function which renders output when service worker is built.
 * @return {Object}
 */
export function generateSW(generateSWConfig, render = report) {
  const { swDest, globDirectory } = generateSWConfig;

  if (!swDest) throw new Error('No service worker destination specified');
  if (!globDirectory) throw new Error('No globDirectory specified');

  const doRender = ({ count, size }) =>
    render({ swDest, count, size });

  return {
    name,
    writeBundle(outputOptions, bundle, isWrite) {
      return _generateSw(generateSWConfig)
        .then(doRender)
        .catch(console.error);
    },
  };
}

/**
 * @typedef {Object} InjectManifestConfig - https://developers.google.com/web/tools/workbox/modules/workbox-build#injectmanifest_mode
 * @property {string} swDest - Indicates whether the Courage component is present.
 * @property {string} swSrc - The path to the source service worker file that can contain your own customized code, in addition to containing a match for injectionPointRegexp.
 * @property {string} globDirectory - The base directory you wish to match globPatterns against, relative to the current working directory.
 * @property {Object[]} [additionalManifestEntries] - A list of entries to be precached, in addition to any entries that are generated as part of the build configuration.
 * @property {string} [injectionPoint] - The string to find inside of the swSrc file. Once found, it will be replaced by the generated precache manifest.
 * @property {string} [mode] - If set to 'production', then an optimized service worker bundle that excludes debugging info will be produced. If not explicitly configured here, the process.env.NODE_ENV value will be used, and failing that, it will fall back to 'production'.
 * @property {Boolean} [globFollow] - Determines whether or not symlinks are followed when generating the precache manifest.
 * @property {string[]} [globIgnores] - A set of patterns matching files to always exclude when generating the precache manifest.
 * @property {string[]} [globPatterns] - Files matching against any of these patterns will be included in the precache manifest.
 * @property {Boolean} [globStrict] - If true, an error reading a directory when generating a precache manifest will cause the build to fail. If false, the problematic directory will be skipped.
 * @property {Object} [templatedURLs] - If a URL is rendered generated based on some server-side logic, its contents may depend on multiple files or on some other unique string value.
 * @property {number} [maximumFileSizeToCacheInBytes] - This value can be used to determine the maximum size of files that will be precached. This prevents you from inadvertantly precaching very large files that might have accidentally matched one of your patterns.
 * @property {Object} [dontCacheBustURLsMatching] - Assets that match this regex will be assumed to be uniquely versioned via their URL, and exempted from the normal HTTP cache-busting that's done when populating the precache.
 * @property {Object} [modifyURLPrefix] - A mapping of prefixes that, if present in an entry in the precache manifest, will be replaced with the corresponding value.
 * @property {Array} [manifestTransforms] - One or more ManifestTransform functions, which will be applied sequentially against the generated manifest.
 */

/**
 * Build me a service-worker
 * @param  {InjectManifestConfig} injectManifestConfig
 * @param  {Function} render - Function which renders output when service worker is built.
 * @return {Object}
 */
export function injectManifest(injectManifestConfig, render = report) {
  const { swSrc, swDest, globDirectory, mode } = injectManifestConfig;

  if (!swSrc) throw new Error('No service worker source specified');
  if (!swDest) throw new Error('No service worker destination specified');
  if (!globDirectory) throw new Error('No globDirectory specified');

  const doRender = ({ count, size }) =>
    render({ swDest, count, size });

  return {
    name,
    writeBundle(outputOptions, bundle, isWrite) {
      return _injectManifest(injectManifestConfig)
        .then(doRender)
        .then(() => mode === 'production' && processBundle({ swDest }))
        .catch(console.error);
    },
  };
}

/**
 * Bundles and minifies the generated service worker.
 *
 * @TODO
 * This is a hack to be able to support the `mode` property for `injectManifest` until Workbox decides to support it.
 * Feature is tracked here: https://github.com/GoogleChrome/workbox/issues/2588
 * Once Workbox's `injectManifest` supports this out of the box, we should remove this.
 *
 * @param {{swDest: string}} swDest
 */
const processBundle = async ({ swDest }) => {
  const bundle = await rollup.rollup({
    input: swDest,
    plugins: [
      replace({ 'process.env.NODE_ENV': '"production"' }),
      resolve(),
      terser({ output: { comments: false } }),
    ],
  });
  await bundle.write({
    file: swDest,
    format: 'iife',
  });
};

